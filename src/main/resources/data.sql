-- Create roles
INSERT INTO roles (id, name) VALUES (100, 'ROLE_ADMIN');
INSERT INTO roles (id, name) VALUES (101, 'ROLE_SKULL_STUDENT');
INSERT INTO roles (id, name) VALUES (102, 'ROLE_NORMAL_STUDENT');
INSERT INTO roles (id, name) VALUES (103, 'ROLE_DEACTIVATED_STUDENT');

-- Create users
INSERT INTO account (id, name, last_name, display_name, email, password, whatsapp, photo, birthday, gender, weight, height, goal, plan)
     VALUES (100, 'Admin', 'Admin', 'Admin Admin', 'admin@admin.com.br', '$2a$12$Vb9XWFeFCQGfWF8cKnTN7.FRyx10dMoBJ/DOXJt9bvwXHgJLaIK2u', '5551981314617',
             'https://drive.google.com/uc?export=view&id=1PaP3IS6TULl4eTY1JrVW0n0l5vazWrF3', '1995-12-11', 0, 1.7, 80.5, 0, 0);

INSERT INTO account (id, name, last_name, display_name, email, password, whatsapp, photo, birthday, gender, plan, weight, height, goal, expiration_date)
     VALUES (101, 'Robson', 'Leal', 'Robson Leal', 'robson_leal@outlook.com.br', '$2a$12$Vb9XWFeFCQGfWF8cKnTN7.FRyx10dMoBJ/DOXJt9bvwXHgJLaIK2u', '5551981314617',
             'https://drive.google.com/uc?export=view&id=10M6cQNZxcM-fusRu-SgzlYrS2fOnxGIP', '1995-12-11', 0, 1, 1.7, 80.5, 0, '2025-03-01');

INSERT INTO account (id, name, last_name, display_name, email, password, whatsapp, photo, birthday, gender, plan, weight, height, goal, expiration_date)
     VALUES (102, 'Matheus', 'Maica', 'Maicatheus', 'maicathus@outlook.com.br', '$2a$12$Vb9XWFeFCQGfWF8cKnTN7.FRyx10dMoBJ/DOXJt9bvwXHgJLaIK2u', '5551981314617',
             'https://drive.google.com/uc?export=view&id=187AfMNR0_BC83qiYNVw9A16mRFYTCouP','1995-12-11', 0, 2, 1.7, 80.5, 0, '2024-11-01');

INSERT INTO account (id, name, last_name, display_name, email, password, whatsapp, photo, birthday, gender, plan, weight, height, goal, expiration_date)
     VALUES (103, 'Teste', 'Teste', 'Teste Teste', 'teste@teste.com.br', '$2a$12$Vb9XWFeFCQGfWF8cKnTN7.FRyx10dMoBJ/DOXJt9bvwXHgJLaIK2u', '5551981314617',
             'https://via.placeholder.com/75', '1995-12-11', 0, 2, 1.7, 80.5, 0, '2024-10-01');

-- Create relationship users-roles
INSERT INTO account_roles (account_id, roles_id) VALUES (100, 100);
INSERT INTO account_roles (account_id, roles_id) VALUES (101, 101);
INSERT INTO account_roles (account_id, roles_id) VALUES (102, 102);
INSERT INTO account_roles (account_id, roles_id) VALUES (103, 103);

-- Create Muscle Groups
INSERT INTO muscle_group (id, name) VALUES (100, 'TÓRAX');
INSERT INTO muscle_group (id, name) VALUES (101, 'OMBROS');
INSERT INTO muscle_group (id, name) VALUES (102, 'GLÚTEO');
INSERT INTO muscle_group (id, name) VALUES (103, 'PEITO');

-- Create Routine
-- Favorite Routine
INSERT INTO routine (id, name, goal, difficulty, init_date, end_date) VALUES (100, 'Rotina 1', 0, 0, '2024-04-23', '2024-08-23');
INSERT INTO routine (id, name, goal, difficulty, init_date, end_date) VALUES (102, 'Rotina 2', 0, 0, '2024-04-23', '2024-08-23');

-- User Routine
INSERT INTO routine (id, name, goal, difficulty, init_date, end_date, account_id) VALUES (101, 'Rotina 1', 0, 0, '2024-04-23', '2024-08-23', 101);

-- Create Exercise
INSERT INTO exercise (id,  name, muscle_group_id, link, guide_lines) VALUES (100, 'Remada alta no cross', 101, 'https://www.youtube.com/watch?v=qVdQsMODm0o&ab_channel=FISIOPREV', 'Esse é um exercício que trabalha o ombro');
INSERT INTO exercise (id,  name, muscle_group_id, link, guide_lines) VALUES (101, 'Encolhimento de ombros', 101, 'https://www.youtube.com/watch?v=cZLgyI2OKcs&ab_channel=MyTrainingPRO', 'Esse é um execício que trabalha o ombro');
INSERT INTO exercise (id,  name, muscle_group_id, link, guide_lines) VALUES (102, 'Agachamento', 102, 'https://www.youtube.com/watch?v=mMvNLg3G9TU&ab_channel=Dr.RodrigoLopes-Fisioterapeuta', 'Esse é um exercício que agacha');
INSERT INTO exercise (id,  name, muscle_group_id, link, guide_lines) VALUES (103, 'Levantamento Terra', 102, 'https://www.youtube.com/watch?v=mMvNLg3G9TU&ab_channel=Dr.RodrigoLopes-Fisioterapeuta', 'Esse é um exercício trabalha o corpo todo');
INSERT INTO exercise (id,  name, muscle_group_id, link, guide_lines) VALUES (104, 'Supino Inclinado', 103, 'https://www.youtube.com/watch?v=mMvNLg3G9TU&ab_channel=Dr.RodrigoLopes-Fisioterapeuta', 'Esse é um exercício trabalha o peito');
INSERT INTO exercise (id,  name, muscle_group_id, link, guide_lines) VALUES (105, 'Rosca Direta', 103, 'https://www.youtube.com/watch?v=mMvNLg3G9TU&ab_channel=Dr.RodrigoLopes-Fisioterapeuta', 'Esse é um exercício trabalha o peito');

-- Create ExerciseDetail
INSERT INTO exercise_details (id, sets_repetition_load_interval) VALUES (100, '2,15,30,60');

-- Create Workout
INSERT INTO workout (id, name, info, routine_id) VALUES (100, 'Treino A', 'Observaçoes sobre o treino', 101);
INSERT INTO workout (id, name, info, routine_id) VALUES (101, 'Treino A', 'Observaçoes sobre o treino', 102);

--Create relationship Workkout-Exercise-Detail
INSERT INTO workout_exercise_detail (id, workout_id, exercise_id, exercise_details_id) VALUES (100, 100, 100, 100);
INSERT INTO workout_exercise_detail (id, workout_id, exercise_id, exercise_details_id) VALUES (101, 100, 102, 100);
INSERT INTO workout_exercise_detail (id, workout_id, exercise_id, exercise_details_id) VALUES (102, 101, 100, 100);
INSERT INTO workout_exercise_detail (id, workout_id, exercise_id, exercise_details_id) VALUES (103, 101, 101, 100);
INSERT INTO workout_exercise_detail (id, workout_id, exercise_id, exercise_details_id) VALUES (104, 101, 102, 100);
INSERT INTO workout_exercise_detail (id, workout_id, exercise_id, exercise_details_id) VALUES (105, 101, 103, 100);
INSERT INTO workout_exercise_detail (id, workout_id, exercise_id, exercise_details_id) VALUES (106, 101, 104, 100);
