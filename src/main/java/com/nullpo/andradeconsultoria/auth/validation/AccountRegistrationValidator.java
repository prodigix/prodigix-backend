package com.nullpo.andradeconsultoria.auth.validation;

import com.nullpo.andradeconsultoria.auth.controller.request.AccountRegistrationRequest;
import com.nullpo.andradeconsultoria.controller.exception.FieldMessage;
import com.nullpo.andradeconsultoria.model.Account;
import com.nullpo.andradeconsultoria.repository.AccountRepository;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
public class AccountRegistrationValidator
  implements ConstraintValidator<AccountRegistrationValid, AccountRegistrationRequest> {

  private final AccountRepository accountRepository;

  @Override
  public void initialize(AccountRegistrationValid constraintAnnotation) {
    ConstraintValidator.super.initialize(constraintAnnotation);
  }

  @Override
  public boolean isValid(AccountRegistrationRequest request, ConstraintValidatorContext context) {
    List<FieldMessage> list = new ArrayList<>();

    Optional<Account> account = accountRepository.findByEmail(request.getEmail());

    if (account.isPresent()) {
      list.add(new FieldMessage("email", "Email já cadastrado"));
    }

    for (FieldMessage e : list) {
      context.disableDefaultConstraintViolation();
      context.buildConstraintViolationWithTemplate(e.getMessage()).addPropertyNode(e.getFieldName())
        .addConstraintViolation();
    }

    return list.isEmpty();
  }
}
