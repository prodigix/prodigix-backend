package com.nullpo.andradeconsultoria.auth.controller;

import com.nullpo.andradeconsultoria.auth.controller.request.AccountRegistrationRequest;
import com.nullpo.andradeconsultoria.auth.controller.request.EmailRequest;
import com.nullpo.andradeconsultoria.auth.controller.request.ResetPasswordRequest;
import com.nullpo.andradeconsultoria.auth.controller.request.VerificationRequest;
import com.nullpo.andradeconsultoria.auth.controller.response.AuthResponse;
import com.nullpo.andradeconsultoria.controller.response.AccountFullResponse;

import jakarta.mail.MessagingException;
import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/api/v1")
public interface AuthController {

  @PostMapping("/signin")
  ResponseEntity<AuthResponse> signin(Authentication request);

  @PostMapping("/signup")
  ResponseEntity<AccountFullResponse> signup(@RequestBody @Valid AccountRegistrationRequest request);

  @PostMapping("/password-reset/request")
  ResponseEntity<String> resetPasswordRequest(@RequestBody EmailRequest request) throws MessagingException;

  @PostMapping("/password-reset/verify")
  ResponseEntity<String> verifyCode(@RequestBody VerificationRequest request);

  @PostMapping("/password-reset/reset")
  ResponseEntity<String> resetPassword(@RequestBody ResetPasswordRequest resetPasswordRequest);

}
