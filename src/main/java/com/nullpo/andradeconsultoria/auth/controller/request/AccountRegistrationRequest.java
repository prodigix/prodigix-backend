package com.nullpo.andradeconsultoria.auth.controller.request;

import com.nullpo.andradeconsultoria.auth.validation.AccountRegistrationValid;
import com.nullpo.andradeconsultoria.dto.AccountDTO;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AccountRegistrationValid
public class AccountRegistrationRequest extends AccountDTO {
  private String password;
  private String confirmPassword;
}
