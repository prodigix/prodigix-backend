package com.nullpo.andradeconsultoria.auth.controller;

import com.nullpo.andradeconsultoria.auth.controller.request.AccountRegistrationRequest;
import com.nullpo.andradeconsultoria.auth.controller.request.EmailRequest;
import com.nullpo.andradeconsultoria.auth.controller.request.ResetPasswordRequest;
import com.nullpo.andradeconsultoria.auth.controller.request.VerificationRequest;
import com.nullpo.andradeconsultoria.auth.controller.response.AuthResponse;
import com.nullpo.andradeconsultoria.auth.service.AuthService;
import com.nullpo.andradeconsultoria.controller.response.AccountFullResponse;
import com.nullpo.andradeconsultoria.service.AccountService;

import jakarta.mail.MessagingException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

@RestController
@RequiredArgsConstructor
@Slf4j
public class AuthControllerImpl implements AuthController {

  private final AuthService authService;
  private final AccountService accountService;

  public ResponseEntity<AuthResponse> signin(Authentication authentication) {
    return ResponseEntity.ok(authService.authenticate(authentication));
  }

  public ResponseEntity<AccountFullResponse> signup(AccountRegistrationRequest request) {
    URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
      .buildAndExpand(request.getId()).toUri();

    return ResponseEntity.created(uri).body(accountService.create(request));
  }

  @Override
  public ResponseEntity<String> resetPasswordRequest(EmailRequest email) throws MessagingException {
    accountService.requestPasswordReset(email);

    return ResponseEntity.ok("Código enviado com sucesso!");
  }

  @Override
  public ResponseEntity<String> verifyCode(VerificationRequest request) {
    accountService.verifyCode(request.getEmail(), request.getCode());

    return ResponseEntity.ok("Código verificado com sucesso.");
  }

  @Override
  public ResponseEntity<String> resetPassword(ResetPasswordRequest request) {
    accountService.verifyCode(request.getEmail(), request.getCode());
    accountService.resetPassword(request.getEmail(), request.getNewPassword());

    return ResponseEntity.ok("Senha redefinida com sucesso.");
  }

}
