package com.nullpo.andradeconsultoria.auth.service;

import com.nullpo.andradeconsultoria.auth.model.CustomUserDetails;
import com.nullpo.andradeconsultoria.repository.AccountRepository;
import com.nullpo.andradeconsultoria.auth.exception.AuthenticationAccountException;
import com.nullpo.andradeconsultoria.model.Account;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {

  private final AccountRepository accountRepository;

  @Override
  public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
    Account account = accountRepository.findByEmail(email)
      .orElseThrow(() -> new AuthenticationAccountException("Invalid email/password"));

    return new CustomUserDetails(account);
  }
}
