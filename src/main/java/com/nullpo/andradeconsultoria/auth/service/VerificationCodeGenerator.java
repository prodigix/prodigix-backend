package com.nullpo.andradeconsultoria.auth.service;

import lombok.NoArgsConstructor;

import java.security.SecureRandom;

@NoArgsConstructor(access = lombok.AccessLevel.PRIVATE)
public class VerificationCodeGenerator {

  public static String generateCode() {
    SecureRandom random = new SecureRandom();
    int code = random.nextInt(999999);
    return String.format("%06d", code);
  }
}
