package com.nullpo.andradeconsultoria.auth.service;

import com.nullpo.andradeconsultoria.auth.controller.response.AuthResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthService {

  private final JwtService jwtService;

  public AuthResponse authenticate(Authentication authentication) {
    return new AuthResponse(jwtService.generateToken(authentication));
  }
}
