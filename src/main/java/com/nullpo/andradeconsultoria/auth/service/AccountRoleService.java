package com.nullpo.andradeconsultoria.auth.service;

import com.nullpo.andradeconsultoria.auth.model.AccountRole;
import com.nullpo.andradeconsultoria.auth.repository.AccountRoleRepository;
import com.nullpo.andradeconsultoria.exception.ResourceNotFoundException;
import com.nullpo.andradeconsultoria.model.enumeration.Plans;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AccountRoleService {

  private final AccountRoleRepository accountRoleRepository;

  public AccountRole getRoleByPlan(Plans plan) {
    return accountRoleRepository.findByName(plan.getValue())
      .orElseThrow(() -> new ResourceNotFoundException("Entity not found: " + plan.getValue()));
  }

}
