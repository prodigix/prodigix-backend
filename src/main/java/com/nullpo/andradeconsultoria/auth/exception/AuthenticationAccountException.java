package com.nullpo.andradeconsultoria.auth.exception;

public class AuthenticationAccountException extends RuntimeException {
  public AuthenticationAccountException(String message) {
    super(message);
  }
}
