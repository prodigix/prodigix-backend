package com.nullpo.andradeconsultoria.auth.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.LocalDateTime;

@Getter
@AllArgsConstructor
public class VerificationCode {
  private String code;
  private LocalDateTime expirationTime;
}
