package com.nullpo.andradeconsultoria.service;

import com.nullpo.andradeconsultoria.auth.controller.request.AccountRegistrationRequest;
import com.nullpo.andradeconsultoria.auth.controller.request.EmailRequest;
import com.nullpo.andradeconsultoria.auth.exception.AuthenticationAccountException;
import com.nullpo.andradeconsultoria.auth.model.VerificationCode;
import com.nullpo.andradeconsultoria.auth.service.AccountRoleService;
import com.nullpo.andradeconsultoria.auth.service.VerificationCodeGenerator;
import com.nullpo.andradeconsultoria.controller.request.AccountUpdateRequest;
import com.nullpo.andradeconsultoria.controller.request.RequestAccountUpdatePlan;
import com.nullpo.andradeconsultoria.controller.response.AccountFullResponse;
import com.nullpo.andradeconsultoria.controller.response.AccountMinimalResponse;
import com.nullpo.andradeconsultoria.controller.response.AccountResponse;
import com.nullpo.andradeconsultoria.dto.AccountDTO;
import com.nullpo.andradeconsultoria.exception.BusinessException;
import com.nullpo.andradeconsultoria.exception.MappingException;
import com.nullpo.andradeconsultoria.exception.ResourceNotFoundException;
import com.nullpo.andradeconsultoria.model.Account;
import com.nullpo.andradeconsultoria.model.Routine;
import com.nullpo.andradeconsultoria.model.enumeration.Plans;
import com.nullpo.andradeconsultoria.repository.AccountRepository;

import jakarta.mail.MessagingException;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.lang.reflect.Field;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

@Service
@RequiredArgsConstructor
public class AccountService {

  private final AccountRepository accountRepository;
  private final ImageAWSS3Service imageAWSS3Service;
  private final AccountRoleService accountRoleService;
  private final ModelMapper modelMapper;
  private final PasswordEncoder passwordEncoder;

  private final ConcurrentHashMap<String, VerificationCode> verificationCodes = new ConcurrentHashMap<>();
  private final EmailService emailService;

  public AccountMinimalResponse listAllStudents() {
    return new AccountMinimalResponse(accountRepository.findByPlanIsNot(Plans.ADMIN));
  }

  @Transactional
  public Account getById(Long id) {
    Account account = accountRepository.findById(id)
      .orElseThrow(() -> new ResourceNotFoundException("Resource not found | AccountId: " + id));
    account.getRoutines().forEach(routine -> routine.getWorkouts().size());
    return account;
  }
  public Account getByEmail(String email) {
    return accountRepository.findByEmail(email)
      .orElseThrow(() -> new ResourceNotFoundException("Resource not found | Email: " + email));
  }

  @Transactional
  public AccountResponse getByIdReturnResponse(Long id) {
    Account account = getById(id);
    account.getRoutines().forEach(routine -> routine.getWorkouts().size());
    return modelMapper.map(account, AccountResponse.class);
  }


  public Account validateRoutineToSetAccount(Routine routine, Long accountId) {
    Account account = getById(accountId);
    List<Routine> routines = new ArrayList<>(account.getRoutines());

    if (routine.getId() != null) {
      routines.removeIf(r -> r.getId().equals(routine.getId()));
    }

    if (routines.isEmpty()) {
      return account;
    }

    routines.sort(Comparator.comparing(Routine::getInitDate));

    Routine lastRoutine = routines.getLast();
    if (lastRoutine.getEndDate().isBefore(routine.getInitDate())) {
      return account;
    }

    throw new BusinessException("ERR004 - Routine already defined for the period");
  }

  public AccountFullResponse create(AccountRegistrationRequest request) {
    Account entity = modelMapper.map(request, Account.class);
    entity.setEmail(entity.getEmail().toLowerCase());
    entity.setPassword(passwordEncoder.encode(request.getPassword()));
    entity.setExpirationDate(LocalDate.now());
    entity.setPlan(Plans.EXPIRED);
    entity.setRoles(Set.of(accountRoleService.getRoleByPlan(entity.getPlan())));

    return modelMapper.map(accountRepository.save(entity), AccountFullResponse.class);
  }

  @Transactional
  public AccountFullResponse updatePlan(Long id, RequestAccountUpdatePlan request) {
    Account entity = getById(id);

    entity.setExpirationDate(request.getExpirationDate());
    entity.setPlan(request.getPlan());

    entity.setRoles(new HashSet<>(Collections.singleton(accountRoleService.getRoleByPlan(request.getPlan()))));

    entity.getRoutines().forEach(routine -> routine.getWorkouts().size());

    return modelMapper.map(accountRepository.save(entity), AccountFullResponse.class);
  }

  public AccountFullResponse updateAccountLoggedAccount(AccountUpdateRequest request) {
    Account entity = getAuthenticatedAccount();
    updateNonNullFields(request, entity);
    return modelMapper.map(accountRepository.save(entity), AccountFullResponse.class);
  }

  public AccountFullResponse updateAccountById(Long id, AccountDTO request) {
    Account entity = getById(id);
    BeanUtils.copyProperties(request, entity, "id");
    return modelMapper.map(accountRepository.save(entity), AccountFullResponse.class);
  }

  public Account getAuthenticatedAccount() {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    if (authentication.getName() != null) {
      return accountRepository.findByEmail(authentication.getName())
        .orElseThrow(() -> new UsernameNotFoundException("User not found!"));
    }

    throw new AuthenticationAccountException("Authentication error");
  }

  @Transactional
  public AccountFullResponse getAuthenticatedAccountReturningAccountFullResponse() {
    Account account = getAuthenticatedAccount();
    account.getRoutines().forEach(routine -> routine.getWorkouts().size());
    return modelMapper.map(account, AccountFullResponse.class);
  }


  public Map<String, String> uploadPerfilImage(MultipartFile file) {
    return imageAWSS3Service.uploadPerfilImage(file);
  }

  public void requestPasswordReset(EmailRequest request) throws MessagingException {
    getByEmail(request.getEmail());

    String code = VerificationCodeGenerator.generateCode();
    verificationCodes.put(request.getEmail(), new VerificationCode(code, LocalDateTime.now().plusMinutes(15)));

    emailService.sendVerificationCode(request.getEmail(), code);
  }

  public void verifyCode(String email, String code) {
    VerificationCode storedCode = verificationCodes.get(email);

    if (!isValidCode(email, code, storedCode))
      throw new BusinessException("Código inválido ou expirado");
  }

  private boolean isValidCode(String email, String code, VerificationCode storedCode) {
    boolean validation = false;

    if (storedCode == null) {
      return validation;
    }

    if (storedCode.getExpirationTime().isBefore(LocalDateTime.now())) {
      verificationCodes.remove(email);
      return validation;
    }

    if (storedCode.getCode().equals(code)) {
      validation = true;
    }

    return validation;
  }

  public void resetPassword(String email, String newPassword) {
    Account account = getByEmail(email);

    String hashedPassword = new BCryptPasswordEncoder().encode(newPassword);
    account.setPassword(hashedPassword);

    accountRepository.save(account);
  }

  private void updateNonNullFields(Object source, Object target) {
    Field[] fields = source.getClass().getDeclaredFields();

    for (Field field : fields) {
      try {
        field.setAccessible(true);
        Object value = field.get(source);

        if (value != null) {
          Field targetField = target.getClass().getDeclaredField(field.getName());
          targetField.setAccessible(true);
          targetField.set(target, value);
        }
      } catch (NoSuchFieldException | IllegalAccessException e) {
        throw new MappingException("Problema ao ");
      }
    }
  }
}
