package com.nullpo.andradeconsultoria.service;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.nullpo.andradeconsultoria.exception.BusinessException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Service
public class ImageAWSS3Service {

  private static final Set<String> SUPPORTED_IMAGE_TYPES = Set.of(".jpg", ".jpeg", ".png", ".gif", ".bmp", ".webp",
    ".svg", ".ico", ".tiff", ".tif", ".heif", ".heic");

  @Value("${aws.accessKeyId}")
  private String accessKeyId;

  @Value("${aws.secretKey}")
  private String secretKey;

  @Value("${aws.s3.bucketName}")
  private String bucketName;

  public Map<String, String> uploadPerfilImage(MultipartFile file) {
    if (isImageValid(file)) {
      return uploadImageS3BucketAndReturnLink(file);
    }

    throw new BusinessException("ERR002 - Invalid imagem type");
  }

  private HashMap<String, String> uploadImageS3BucketAndReturnLink(MultipartFile file) {
    AmazonS3 s3Client = getAmazonClient();
    String fileName = SecurityContextHolder.getContext().getAuthentication().getName();

    try {
      ObjectMetadata metadata = new ObjectMetadata();
      metadata.setContentLength(file.getSize());
      s3Client.putObject(new PutObjectRequest(bucketName, fileName, file.getInputStream(), metadata));
      String imageUrl = s3Client.getUrl(bucketName, fileName).toString();
      return new HashMap<>(Map.of("imageUrl", imageUrl));
    } catch (IOException e) {
      throw new InternalError("ERR003 - Upload image error");
    }
  }

  public boolean isImageValid(MultipartFile file) {
    String fileOriginalName = file.getOriginalFilename();
    boolean endsWithSupportedType = false;

    if (!file.isEmpty()) {
      endsWithSupportedType = SUPPORTED_IMAGE_TYPES.stream()
        .anyMatch(type -> {
          assert fileOriginalName != null;
          return fileOriginalName.toLowerCase().endsWith(type);
        });
    }

    return endsWithSupportedType;
  }

  private AmazonS3 getAmazonClient() {
    BasicAWSCredentials credentials = new BasicAWSCredentials(accessKeyId, secretKey);

    return AmazonS3ClientBuilder.standard()
      .withCredentials(new AWSStaticCredentialsProvider(credentials))
      .withRegion(Regions.US_EAST_1)
      .build();
  }
}
