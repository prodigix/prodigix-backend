package com.nullpo.andradeconsultoria.service;

import com.nullpo.andradeconsultoria.controller.request.CreateRoutineRequest;
import com.nullpo.andradeconsultoria.controller.response.RoutineFullResponse;
import com.nullpo.andradeconsultoria.dto.RoutineDTO;
import com.nullpo.andradeconsultoria.exception.ResourceNotFoundException;
import com.nullpo.andradeconsultoria.model.Account;
import com.nullpo.andradeconsultoria.model.Routine;
import com.nullpo.andradeconsultoria.model.Workout;
import com.nullpo.andradeconsultoria.model.WorkoutExerciseDetail;
import com.nullpo.andradeconsultoria.repository.RoutineRepository;
import com.nullpo.andradeconsultoria.repository.WorkoutRepository;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class RoutineService {

  private final RoutineRepository routineRepository;
  private final WorkoutRepository workoutRepository;
  private final WorkoutExerciceDetailService workoutExerciceDetailService;
  private final AccountService accountService;
  private final ModelMapper modelMapper;

  @PersistenceContext
  private EntityManager entityManager;

  @Transactional
  public RoutineDTO create(CreateRoutineRequest request) {
    Routine entity = modelMapper.map(request, Routine.class);

    if (request.getAccountId() != null) {
      entity.setAccount(accountService.validateRoutineToSetAccount(entity, request.getAccountId()));
    }

    return modelMapper.map(routineRepository.save(entity), RoutineDTO.class);
  }

  public List<RoutineDTO> listAllFavorites() {
    return routineRepository.findByAccountIsNull().stream()
      .map(routine -> modelMapper.map(routine, RoutineDTO.class)).toList();
  }

  public Routine getById(Long id) {
    return routineRepository.findById(id)
      .orElseThrow(() -> new ResourceNotFoundException("Resource not found | RoutineId: " + id));
  }

  @Transactional
  public RoutineFullResponse getByIdReturnFullResponse(Long id) {
    Routine routine = getById(id);

    routine.getWorkouts().forEach(Workout::getWorkoutExerciseDetail);

    return modelMapper.map(routine, RoutineFullResponse.class);
  }


  public RoutineDTO update(Long id, CreateRoutineRequest request) {
    Routine entity = getById(id);
    BeanUtils.copyProperties(request, entity, "id", "accountId");
    accountService.validateRoutineToSetAccount(entity, request.getAccountId());
    return modelMapper.map(routineRepository.save(entity), RoutineDTO.class);
  }

  @Transactional
  public RoutineDTO cloneRoutineAndLinkAccount(Long routineId, Long accountId) {
    Account account = null;
    if (accountId > 0) {
      account = accountService.getById(accountId);
    }
    Routine routine = getById(routineId);

    Routine clonedRoutine = cloneRoutineWithoutReferences(routine, account);

    for (Workout workout : routine.getWorkouts()) {
      Workout clonedWorkout = cloneWorkoutWithoutReferences(workout, clonedRoutine);
      clonedRoutine.getWorkouts().add(clonedWorkout);
    }

    return modelMapper.map(clonedRoutine, RoutineDTO.class);
  }

  private Workout cloneWorkoutWithoutReferences(Workout workout, Routine clonedRoutine) {
    Workout clonedWorkout = new Workout();
    clonedWorkout.setName(workout.getName());
    clonedWorkout.setInfo(workout.getInfo());
    clonedWorkout.setRoutine(clonedRoutine);

    Set<WorkoutExerciseDetail> clonedDetails = workout.getWorkoutExerciseDetail().stream()
      .map(wed -> cloneWorkoutExerciseDetail(wed, clonedWorkout))
      .collect(Collectors.toSet());
    clonedWorkout.setWorkoutExerciseDetail(clonedDetails);

    return workoutRepository.save(clonedWorkout);
  }

  private WorkoutExerciseDetail cloneWorkoutExerciseDetail(WorkoutExerciseDetail wed, Workout clonedWorkout) {
    WorkoutExerciseDetail clonedWed = new WorkoutExerciseDetail();
    clonedWed.setWorkout(clonedWorkout);
    clonedWed.setExercise(wed.getExercise());
    clonedWed.setDetails(wed.getDetails());
    return workoutExerciceDetailService.save(clonedWed);
  }

  private Routine cloneRoutineWithoutReferences(Routine routine, Account account) {
    Routine clonedRoutine = new Routine();
    Set<Workout> initialSetEmptyWorkouts = new HashSet<>();
    clonedRoutine.setName(routine.getName());
    clonedRoutine.setGoal(routine.getGoal());
    clonedRoutine.setDifficulty(routine.getDifficulty());
    clonedRoutine.setAccount(account);
    clonedRoutine.setWorkouts(initialSetEmptyWorkouts);
    return routineRepository.save(clonedRoutine);
  }

  @Transactional
  public void delete(Long id) {
    Routine routine = getById(id);
    for (Workout workout : routine.getWorkouts()) {
      workoutRepository.deleteWorkoutExerciseDetailsByWorkoutId(workout.getId());
      workoutRepository.deleteWorkout(workout.getId());
    }
    entityManager.flush();
    entityManager.clear();
    routineRepository.deleteById(id);
  }
}
