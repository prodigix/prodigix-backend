package com.nullpo.andradeconsultoria.service;

import com.nullpo.andradeconsultoria.dto.MuscleGroupDTO;
import com.nullpo.andradeconsultoria.exception.ResourceNotFoundException;
import com.nullpo.andradeconsultoria.repository.MuscleGroupRepository;
import com.nullpo.andradeconsultoria.model.MuscleGroup;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class MuscleGroupService {

  private final MuscleGroupRepository repository;
  private final ModelMapper modelMapper;

  public MuscleGroup findByName(String name) {
    String nameUpperCase = name.toUpperCase();
    return repository.findByName(nameUpperCase)
      .orElseThrow(() -> new ResourceNotFoundException("Resource not found | MuscleGroupName: " + name));
  }

  public MuscleGroupDTO create(MuscleGroupDTO dto) {
    MuscleGroup entity = new MuscleGroup();
    entity.setName(dto.getName().toUpperCase());
    return modelMapper.map(repository.save(entity), MuscleGroupDTO.class);
  }

  public List<MuscleGroupDTO> listAll() {
    return repository.findAll().stream().map(element -> modelMapper.map(element, MuscleGroupDTO.class)).toList();
  }

  public List<MuscleGroupDTO> filterName(String name) {
    return repository.findByNameContaining(name);
  }
}
