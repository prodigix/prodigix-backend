package com.nullpo.andradeconsultoria.service;

import lombok.RequiredArgsConstructor;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;

@Service
@RequiredArgsConstructor
public class EmailService {

  private final JavaMailSender mailSender;

  public void sendVerificationCode(String toEmail, String code) throws MessagingException {
    MimeMessage message = mailSender.createMimeMessage();
    MimeMessageHelper helper = new MimeMessageHelper(message, true);

    helper.setTo(toEmail);
    helper.setSubject("CONSULTORIA ANDRADE - Código de redefinição de senha");
    helper.setFrom("cassio_andrade@andradeconsultoria.com");

    String htmlContent = "<p>Seu código de verificação é: <b>" + code + "</b></p>"
            + "<p>O código tem validade de <b>15 minutos</b>.</p>";
    helper.setText(htmlContent, true);

    mailSender.send(message);
  }
}
