package com.nullpo.andradeconsultoria.service;

import com.nullpo.andradeconsultoria.dto.ExerciseDetailsDTO;
import com.nullpo.andradeconsultoria.model.ExerciseDetails;
import com.nullpo.andradeconsultoria.repository.ExerciseDetailsRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ExerciseDetailsService {

  private final ExerciseDetailsRepository repository;
  private final ModelMapper modelMapper;

  public ExerciseDetails getOrCreate(ExerciseDetailsDTO dto) {
    ExerciseDetails entity = modelMapper.map(dto, ExerciseDetails.class);
    return repository.findBySetsRepetitionLoadInterval(entity.getSetsRepetitionLoadInterval())
      .orElseGet(() -> repository.save(entity));
  }

}
