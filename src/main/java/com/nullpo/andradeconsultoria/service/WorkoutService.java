package com.nullpo.andradeconsultoria.service;

import com.nullpo.andradeconsultoria.controller.request.CreateWorkoutRequest;
import com.nullpo.andradeconsultoria.controller.request.ExerciseDetailsRequest;
import com.nullpo.andradeconsultoria.controller.response.ExerciseDetailsResponse;
import com.nullpo.andradeconsultoria.controller.response.WorkoutMinimalResponse;
import com.nullpo.andradeconsultoria.dto.ExerciseDetailsDTO;
import com.nullpo.andradeconsultoria.dto.WorkoutDTO;
import com.nullpo.andradeconsultoria.exception.ResourceNotFoundException;
import com.nullpo.andradeconsultoria.model.Exercise;
import com.nullpo.andradeconsultoria.model.ExerciseDetails;
import com.nullpo.andradeconsultoria.model.Workout;
import com.nullpo.andradeconsultoria.model.WorkoutExerciseDetail;
import com.nullpo.andradeconsultoria.repository.WorkoutRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class WorkoutService {

  private static final String RESOURCE_NAME_EXCEPTION = "WorkoutId: ";

  private final WorkoutRepository workoutRepository;
  private final ExerciseDetailsService exerciseDetailsService;
  private final WorkoutExerciceDetailService workoutExerciceDetailService;
  private final ExerciseService exerciseService;
  private final ModelMapper modelMapper;

  @Transactional
  public WorkoutMinimalResponse create(CreateWorkoutRequest request) {
    Workout workout = workoutRepository.save(modelMapper.map(request, Workout.class));

    for (ExerciseDetailsRequest exerciseDetailsRequest : request.getExercises()) {
      ExerciseDetails exerciseDetails = exerciseDetailsService.getOrCreate(exerciseDetailsRequest.getDetails());
      Exercise exercise = exerciseService.getById(exerciseDetailsRequest.getId());
      WorkoutExerciseDetail workoutExerciseDetail = new WorkoutExerciseDetail(null, workout, exercise, exerciseDetails);
      workoutExerciceDetailService.save(workoutExerciseDetail);
    }

    request.setId(workout.getId());
    return modelMapper.map(request, WorkoutMinimalResponse.class);
  }

  public WorkoutDTO getById(Long id) {
    Workout workout = workoutRepository.findById(id)
      .orElseThrow(() -> new ResourceNotFoundException(RESOURCE_NAME_EXCEPTION, id));

    WorkoutDTO response = modelMapper.map(workout, WorkoutDTO.class);
    response.setExercises(workout.getWorkoutExerciseDetail().stream().map(workoutExerciseDetail -> {
      ExerciseDetailsResponse exerciseDetailsResponse = new ExerciseDetailsResponse();
      exerciseDetailsResponse.setId(workoutExerciseDetail.getExercise().getId());
      exerciseDetailsResponse.setName(workoutExerciseDetail.getExercise().getName());
      exerciseDetailsResponse.setLink(workoutExerciseDetail.getExercise().getLink());
      exerciseDetailsResponse.setDetails(modelMapper.map(workoutExerciseDetail.getDetails(), ExerciseDetailsDTO.class));
      return exerciseDetailsResponse;
    }).collect(Collectors.toSet()));

    return response;
  }

  @Transactional
  public WorkoutMinimalResponse update(Long id, CreateWorkoutRequest request) {
    Workout workout = workoutRepository.findById(id)
      .orElseThrow(() -> new ResourceNotFoundException(RESOURCE_NAME_EXCEPTION, id));
    workout.setName(request.getName());
    workout.setInfo(request.getInfo());
    workoutRepository.deleteWorkoutExerciseDetailsByWorkoutId(workout.getId());

    for (ExerciseDetailsRequest exerciseDetailsRequest : request.getExercises()) {
      ExerciseDetails exerciseDetails = exerciseDetailsService.getOrCreate(exerciseDetailsRequest.getDetails());
      Exercise exercise = exerciseService.getById(exerciseDetailsRequest.getId());
      WorkoutExerciseDetail workoutExerciseDetail = new WorkoutExerciseDetail(null, workout, exercise, exerciseDetails);
      workoutExerciceDetailService.save(workoutExerciseDetail);
    }

    request.setId(id);
    return modelMapper.map(request, WorkoutMinimalResponse.class);
  }

  @Transactional
  public void delete(Long id) {
    Workout workout = workoutRepository.findById(id)
      .orElseThrow(() -> new ResourceNotFoundException(RESOURCE_NAME_EXCEPTION, id));

    workoutRepository.deleteWorkoutExerciseDetailsByWorkoutId(workout.getId());
    workoutRepository.deleteWorkout(workout.getId());
  }

  public void updateWorkoutExerciseDetail(Long workoutId, Long exerciseId, ExerciseDetailsDTO request) {
    workoutExerciceDetailService.updateWorkoutExerciseDetails(workoutId, exerciseId, request);
  }

  public void updateAllDetailsOfWorkoutExercises(Long workoutId, ExerciseDetailsDTO request) {
    workoutExerciceDetailService.updateAllDetailsOfWorkoutExercises(workoutId, request);
  }
}
