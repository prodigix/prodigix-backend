package com.nullpo.andradeconsultoria.service;

import com.nullpo.andradeconsultoria.dto.ExerciseDTO;
import com.nullpo.andradeconsultoria.exception.BusinessException;
import com.nullpo.andradeconsultoria.exception.ResourceNotFoundException;
import com.nullpo.andradeconsultoria.model.Exercise;
import com.nullpo.andradeconsultoria.repository.ExerciseRepository;
import com.nullpo.andradeconsultoria.repository.projection.ExerciseMinimalProjection;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ExerciseService {

  private static final String MESSAGE_EXERCISE_NOT_FOUNT = "Resource not found | ExerciseId: ";

  private final ExerciseRepository exerciseRepository;
  private final MuscleGroupService muscleGroupService;
  private final ModelMapper modelMapper;

  public ExerciseDTO create(ExerciseDTO request) {
    Exercise entity = modelMapper.map(request, Exercise.class);
    entity.setMuscleGroup(muscleGroupService.findByName(request.getMuscleGroup()));
    return modelMapper.map(exerciseRepository.save(entity), ExerciseDTO.class);
  }


  public ExerciseDTO update(Long id, ExerciseDTO request) {
    Exercise entity = exerciseRepository.findById(id)
      .orElseThrow(() -> new ResourceNotFoundException(MESSAGE_EXERCISE_NOT_FOUNT + id));
    request.setId(id);
    BeanUtils.copyProperties(request, entity, "muscleGroup");
    entity.setMuscleGroup(muscleGroupService.findByName(request.getMuscleGroup()));
    return modelMapper.map(exerciseRepository.save(entity), ExerciseDTO.class);
  }

  public Exercise getById(Long id) {
    return exerciseRepository.findById(id)
      .orElseThrow(() -> new ResourceNotFoundException(MESSAGE_EXERCISE_NOT_FOUNT + id));
  }

  public ExerciseDTO getByIdRetuningDTO(Long id) {
    return modelMapper.map(getById(id), ExerciseDTO.class);
  }

  public List<ExerciseMinimalProjection> listAllOrFilter(Long ignoredWorkoutId) {
    if (ignoredWorkoutId != null) {
      return exerciseRepository.findAllNotInWorkout(ignoredWorkoutId);
    } else {
      return exerciseRepository.findAllMinimal();
    }
  }

  public void delete(Long id) {
    Exercise exercise = exerciseRepository.findById(id)
      .orElseThrow(() -> new ResourceNotFoundException(MESSAGE_EXERCISE_NOT_FOUNT + id));

    try {
      exerciseRepository.delete(exercise);
    } catch (DataIntegrityViolationException e) {
      throw new BusinessException("Exercise has associated item");
    }
  }

}
