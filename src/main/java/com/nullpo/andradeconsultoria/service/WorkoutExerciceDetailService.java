package com.nullpo.andradeconsultoria.service;

import com.nullpo.andradeconsultoria.dto.ExerciseDetailsDTO;
import com.nullpo.andradeconsultoria.model.ExerciseDetails;
import com.nullpo.andradeconsultoria.model.WorkoutExerciseDetail;
import com.nullpo.andradeconsultoria.repository.WorkoutExerciceDetailsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class WorkoutExerciceDetailService {

  private final WorkoutExerciceDetailsRepository repository;
  private final ExerciseDetailsService exerciseDetailsService;

  @Transactional
  public WorkoutExerciseDetail save(WorkoutExerciseDetail entity) {
    return repository.save(entity);
  }

  public void delete(WorkoutExerciseDetail entity) {
    repository.delete(entity);
  }

  public void updateWorkoutExerciseDetails(Long workoutId, Long exerciseId, ExerciseDetailsDTO exerciseDetailsDTO) {
    WorkoutExerciseDetail wed = repository.findByWorkoutIdAndExerciseId(workoutId, exerciseId);
    ExerciseDetails exerciseDetails = exerciseDetailsService.getOrCreate(exerciseDetailsDTO);
    wed.setDetails(exerciseDetails);
    repository.save(wed);
  }

  public void updateAllDetailsOfWorkoutExercises(Long workoutId, ExerciseDetailsDTO exerciseDetailsDTO) {
    List<WorkoutExerciseDetail> wedList = repository.findByWorkoutId(workoutId);
    ExerciseDetails exerciseDetails = exerciseDetailsService.getOrCreate(exerciseDetailsDTO);
    for (WorkoutExerciseDetail wed : wedList) {
      wed.setDetails(exerciseDetails);
      repository.save(wed);
    }
  }

}

