package com.nullpo.andradeconsultoria.controller.impl;

import com.nullpo.andradeconsultoria.controller.ExerciseController;
import com.nullpo.andradeconsultoria.dto.ExerciseDTO;
import com.nullpo.andradeconsultoria.repository.projection.ExerciseMinimalProjection;
import com.nullpo.andradeconsultoria.service.ExerciseService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
@RequiredArgsConstructor
public class ExerciseControllerImpl implements ExerciseController {

  private final ExerciseService service;

  @Override
  public ResponseEntity<ExerciseDTO> create(ExerciseDTO request) {
    URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
      .buildAndExpand(request.getId()).toUri();

    return ResponseEntity.created(uri).body(service.create(request));
  }

  @Override
  public ResponseEntity<ExerciseDTO> update(Long id, ExerciseDTO request) {
    return ResponseEntity.ok(service.update(id, request));
  }

  @Override
  public ResponseEntity<ExerciseDTO> getById(Long id) {
    return ResponseEntity.ok(service.getByIdRetuningDTO(id));
  }

  @Override
  public ResponseEntity<List<ExerciseMinimalProjection>> listAll(Long ignoredWorkoutId) {
    return ResponseEntity.ok(service.listAllOrFilter(ignoredWorkoutId));
  }

  @Override
  public ResponseEntity<Void> delete(Long id) {
    service.delete(id);
    return ResponseEntity.ok().build();
  }
}
