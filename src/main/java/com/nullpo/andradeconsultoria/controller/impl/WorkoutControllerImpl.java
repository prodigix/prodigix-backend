package com.nullpo.andradeconsultoria.controller.impl;

import com.nullpo.andradeconsultoria.controller.WorkoutController;
import com.nullpo.andradeconsultoria.controller.request.CreateWorkoutRequest;
import com.nullpo.andradeconsultoria.controller.response.WorkoutMinimalResponse;
import com.nullpo.andradeconsultoria.dto.ExerciseDetailsDTO;
import com.nullpo.andradeconsultoria.dto.WorkoutDTO;
import com.nullpo.andradeconsultoria.service.WorkoutService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

@RestController
@RequiredArgsConstructor
public class WorkoutControllerImpl implements WorkoutController {

  private final WorkoutService service;

  @Override
  public ResponseEntity<WorkoutMinimalResponse> create(CreateWorkoutRequest request) {
    URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(request.getId()).toUri();

    return ResponseEntity.created(uri).body(service.create(request));
  }

  @Override
  public ResponseEntity<WorkoutDTO> getById(Long id) {
    return ResponseEntity.ok(service.getById(id));
  }

  @Override
  public ResponseEntity<WorkoutMinimalResponse> updateById(Long id, CreateWorkoutRequest request) {
    return ResponseEntity.ok(service.update(id, request));
  }

  @Override
  public ResponseEntity<Void> deleteById(Long id) {
    service.delete(id);
    return ResponseEntity.noContent().build();
  }

  @Override
  public ResponseEntity<Void> updateExerciseDetail(Long workoutId,
                                                   Long exerciseId,
                                                   ExerciseDetailsDTO request,
                                                   boolean replicate) {

    if (replicate) {
      service.updateAllDetailsOfWorkoutExercises(workoutId, request);
    } else {
      service.updateWorkoutExerciseDetail(workoutId, exerciseId, request);
    }

    return ResponseEntity.noContent().build();
  }

}
