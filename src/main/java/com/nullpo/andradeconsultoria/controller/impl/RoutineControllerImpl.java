package com.nullpo.andradeconsultoria.controller.impl;

import com.nullpo.andradeconsultoria.controller.RoutineController;
import com.nullpo.andradeconsultoria.controller.request.CreateRoutineRequest;
import com.nullpo.andradeconsultoria.controller.response.RoutineFullResponse;
import com.nullpo.andradeconsultoria.dto.RoutineDTO;
import com.nullpo.andradeconsultoria.service.RoutineService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
@RequiredArgsConstructor
public class RoutineControllerImpl implements RoutineController {

  private final RoutineService service;

  @Override
  public ResponseEntity<RoutineDTO> create(CreateRoutineRequest request) {
    URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(request.getId()).toUri();

    return ResponseEntity.created(uri).body(service.create(request));
  }

  @Override
  public ResponseEntity<List<RoutineDTO>> listAllFavoritesAdmin() {
    return ResponseEntity.ok(service.listAllFavorites());
  }

  @Override
  public ResponseEntity<RoutineFullResponse> getById(Long id) {
    return ResponseEntity.ok(service.getByIdReturnFullResponse(id));
  }

  @Override
  public ResponseEntity<RoutineDTO> update(Long id, CreateRoutineRequest request) {
    return ResponseEntity.ok(service.update(id, request));
  }

  @Override
  public ResponseEntity<RoutineDTO> cloneRoutineAndLinkAccount(Long accountId, Long routineId) {
    return ResponseEntity.ok(service.cloneRoutineAndLinkAccount(routineId, accountId));
  }

  @Override
  public ResponseEntity<Void> deleteById(Long id) {
    service.delete(id);
    return ResponseEntity.noContent().build();
  }

}
