package com.nullpo.andradeconsultoria.controller.impl;

import com.nullpo.andradeconsultoria.controller.AccountController;
import com.nullpo.andradeconsultoria.controller.request.AccountUpdateRequest;
import com.nullpo.andradeconsultoria.controller.request.RequestAccountUpdatePlan;
import com.nullpo.andradeconsultoria.controller.response.AccountMinimalResponse;
import com.nullpo.andradeconsultoria.controller.response.AccountFullResponse;
import com.nullpo.andradeconsultoria.controller.response.AccountResponse;
import com.nullpo.andradeconsultoria.dto.AccountDTO;
import com.nullpo.andradeconsultoria.service.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

@RestController
@RequiredArgsConstructor
public class AccountControllerImpl implements AccountController {

  private final AccountService service;

  @Override
  public ResponseEntity<AccountFullResponse> getAccount() {
    return ResponseEntity.ok(service.getAuthenticatedAccountReturningAccountFullResponse());
  }

  @Override
  public ResponseEntity<AccountFullResponse> updateLoggedAccount(AccountUpdateRequest request) {
    return ResponseEntity.ok(service.updateAccountLoggedAccount(request));
  }

  @Override
  public ResponseEntity<AccountMinimalResponse> listAll() {
    return ResponseEntity.ok(service.listAllStudents());
  }

  @Override
  public ResponseEntity<AccountResponse> getById(Long id) {
    return ResponseEntity.ok(service.getByIdReturnResponse(id));
  }

  @Override
  public ResponseEntity<AccountFullResponse> updateById(Long id, AccountDTO request) {
    return ResponseEntity.ok(service.updateAccountById(id, request));
  }

  @Override
  public ResponseEntity<AccountFullResponse> updatePlan(Long id, RequestAccountUpdatePlan request) {
    return ResponseEntity.ok(service.updatePlan(id, request));
  }

  @Override
  public ResponseEntity<Map<String, String>> uploadPerfilImage(MultipartFile image) {
    return ResponseEntity.ok(service.uploadPerfilImage(image));
  }

}
