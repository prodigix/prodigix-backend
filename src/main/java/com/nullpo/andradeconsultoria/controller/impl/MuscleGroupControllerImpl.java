package com.nullpo.andradeconsultoria.controller.impl;

import com.nullpo.andradeconsultoria.controller.MuscleGroupController;
import com.nullpo.andradeconsultoria.dto.MuscleGroupDTO;
import com.nullpo.andradeconsultoria.service.MuscleGroupService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class MuscleGroupControllerImpl implements MuscleGroupController {

  private final MuscleGroupService muscleGroupService;

  @Override
  public ResponseEntity<List<MuscleGroupDTO>> listAll(String name) {
    if (name != null) {
      return ResponseEntity.ok(muscleGroupService.filterName(name));
    }
    return ResponseEntity.ok(muscleGroupService.listAll());
  }

  @Override
  public ResponseEntity<MuscleGroupDTO> create(MuscleGroupDTO request) {
    return ResponseEntity.ok(muscleGroupService.create(request));
  }

}
