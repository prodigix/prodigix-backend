package com.nullpo.andradeconsultoria.controller;

import com.nullpo.andradeconsultoria.controller.request.CreateWorkoutRequest;
import com.nullpo.andradeconsultoria.controller.response.WorkoutMinimalResponse;
import com.nullpo.andradeconsultoria.dto.ExerciseDetailsDTO;
import com.nullpo.andradeconsultoria.dto.WorkoutDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/api/v1/admin/workouts")
public interface WorkoutController {

  @PostMapping
  ResponseEntity<WorkoutMinimalResponse> create(@RequestBody CreateWorkoutRequest request);

  @GetMapping("/{id}")
  ResponseEntity<WorkoutDTO> getById(@PathVariable Long id);

  @PutMapping("/{id}")
  ResponseEntity<WorkoutMinimalResponse> updateById(@PathVariable Long id, @RequestBody CreateWorkoutRequest request);

  @DeleteMapping("/{id}")
  ResponseEntity<Void> deleteById(@PathVariable Long id);

  @PutMapping("/{workoutId}/exercises/{exerciseId}/details")
  ResponseEntity<Void> updateExerciseDetail(@PathVariable Long workoutId,
                                            @PathVariable Long exerciseId,
                                            @RequestBody ExerciseDetailsDTO request,
                                            @RequestParam boolean replicate);

}
