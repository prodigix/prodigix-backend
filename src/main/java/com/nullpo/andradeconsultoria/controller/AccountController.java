package com.nullpo.andradeconsultoria.controller;

import com.nullpo.andradeconsultoria.controller.request.AccountUpdateRequest;
import com.nullpo.andradeconsultoria.controller.request.RequestAccountUpdatePlan;
import com.nullpo.andradeconsultoria.controller.response.AccountMinimalResponse;
import com.nullpo.andradeconsultoria.controller.response.AccountFullResponse;
import com.nullpo.andradeconsultoria.controller.response.AccountResponse;
import com.nullpo.andradeconsultoria.dto.AccountDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

@RequestMapping("/api/v1")
public interface AccountController {

  @GetMapping("/accounts")
  ResponseEntity<AccountFullResponse> getAccount();

  @PutMapping("/accounts")
  ResponseEntity<AccountFullResponse> updateLoggedAccount(@RequestBody AccountUpdateRequest request);

  @GetMapping("/admin/accounts")
  ResponseEntity<AccountMinimalResponse> listAll();

  @GetMapping("/admin/accounts/{id}")
  ResponseEntity<AccountResponse> getById(@PathVariable Long id);

  @PatchMapping("/admin/accounts/{id}")
  ResponseEntity<AccountFullResponse> updatePlan(@PathVariable Long id, @RequestBody RequestAccountUpdatePlan request);

  @PutMapping("/admin/accounts/{id}")
  ResponseEntity<AccountFullResponse> updateById(@PathVariable Long id, @RequestBody AccountDTO request);

  @PostMapping("/accounts/update-perfil-image")
  ResponseEntity<Map<String, String>> uploadPerfilImage(@RequestParam("image")MultipartFile image);

}
