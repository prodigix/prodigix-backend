package com.nullpo.andradeconsultoria.controller;

import com.nullpo.andradeconsultoria.dto.ExerciseDTO;
import com.nullpo.andradeconsultoria.repository.projection.ExerciseMinimalProjection;
import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/api/v1/admin/exercises")
public interface ExerciseController {

  @PostMapping
  ResponseEntity<ExerciseDTO> create(@RequestBody @Valid ExerciseDTO request);

  @PutMapping("/{id}")
  ResponseEntity<ExerciseDTO> update(@PathVariable Long id, @RequestBody @Valid ExerciseDTO request);

  @GetMapping("/{id}")
  ResponseEntity<ExerciseDTO> getById(@PathVariable Long id);

  @GetMapping
  ResponseEntity<List<ExerciseMinimalProjection>> listAll(@RequestParam(required = false) Long ignoredWorkoutId);

  @DeleteMapping("/{id}")
  ResponseEntity<Void> delete(@PathVariable Long id);

}
