package com.nullpo.andradeconsultoria.controller.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreateWorkoutRequest {
  private Long id;
  private Long routineId;
  private String name;
  private String info;
  private Set<ExerciseDetailsRequest> exercises;
}
