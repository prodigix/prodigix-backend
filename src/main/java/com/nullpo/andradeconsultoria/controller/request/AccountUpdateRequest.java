package com.nullpo.andradeconsultoria.controller.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.nullpo.andradeconsultoria.model.enumeration.Genders;
import com.nullpo.andradeconsultoria.model.enumeration.Goals;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class AccountUpdateRequest {
  private String email;
  private String name;
  private String photo;
  private String lastName;
  private String displayName;
  private String whatsapp;
  private LocalDate birthday;
  private Goals goal;
  private Genders gender;
  private Double height;
  private Double weight;
}
