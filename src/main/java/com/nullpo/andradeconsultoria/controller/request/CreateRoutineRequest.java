package com.nullpo.andradeconsultoria.controller.request;

import com.nullpo.andradeconsultoria.dto.RoutineDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreateRoutineRequest extends RoutineDTO {
  private Long accountId;
}
