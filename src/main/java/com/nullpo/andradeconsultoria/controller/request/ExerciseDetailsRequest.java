package com.nullpo.andradeconsultoria.controller.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.nullpo.andradeconsultoria.dto.ExerciseDetailsDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExerciseDetailsRequest {
  private Long id;
  private ExerciseDetailsDTO details;
}
