package com.nullpo.andradeconsultoria.controller.request;

import java.time.LocalDate;

import com.nullpo.andradeconsultoria.model.enumeration.Plans;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RequestAccountUpdatePlan {
  private Plans plan;
  private LocalDate expirationDate;
}
