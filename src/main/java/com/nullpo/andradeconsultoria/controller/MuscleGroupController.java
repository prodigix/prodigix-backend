package com.nullpo.andradeconsultoria.controller;

import com.nullpo.andradeconsultoria.dto.MuscleGroupDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@RequestMapping("/api/v1/admin/muscleGroups")
public interface MuscleGroupController {

  @GetMapping
  ResponseEntity<List<MuscleGroupDTO>> listAll(@RequestParam(required = false) String filterName);

  @PostMapping
  ResponseEntity<MuscleGroupDTO> create(@RequestBody MuscleGroupDTO request);

}
