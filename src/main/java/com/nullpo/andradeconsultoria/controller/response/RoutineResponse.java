package com.nullpo.andradeconsultoria.controller.response;

import com.nullpo.andradeconsultoria.dto.RoutineDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RoutineResponse extends RoutineDTO {
  Set<WorkoutMinimalResponse> workouts;
}
