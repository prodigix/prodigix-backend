package com.nullpo.andradeconsultoria.controller.response;

import com.nullpo.andradeconsultoria.repository.projection.AccountMinimalProjection;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AccountMinimalResponse {
  private Set<AccountMinimalProjection> students;
}
