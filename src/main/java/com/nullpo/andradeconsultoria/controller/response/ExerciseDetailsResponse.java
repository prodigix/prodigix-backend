package com.nullpo.andradeconsultoria.controller.response;

import com.nullpo.andradeconsultoria.dto.ExerciseDetailsDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ExerciseDetailsResponse {
  private Long id;
  private String name;
  private String muscleGroup;
  private String link;
  private String guideLines;
  private ExerciseDetailsDTO details;
}
