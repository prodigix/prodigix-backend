package com.nullpo.andradeconsultoria.controller.response;

import com.nullpo.andradeconsultoria.dto.AccountDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AccountFullResponse extends AccountDTO {
  private List<RoutineFullResponse> routines;
}
