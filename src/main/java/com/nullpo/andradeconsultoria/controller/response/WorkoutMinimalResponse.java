package com.nullpo.andradeconsultoria.controller.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class WorkoutMinimalResponse {
  private Long id;
  private String name;
  private String info;
}
