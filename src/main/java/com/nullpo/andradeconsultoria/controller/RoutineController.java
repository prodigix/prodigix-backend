package com.nullpo.andradeconsultoria.controller;

import com.nullpo.andradeconsultoria.controller.request.CreateRoutineRequest;
import com.nullpo.andradeconsultoria.controller.response.RoutineFullResponse;
import com.nullpo.andradeconsultoria.dto.RoutineDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@RequestMapping("/api/v1/admin")
public interface RoutineController {

  @PostMapping("/routines")
  ResponseEntity<RoutineDTO> create(@RequestBody CreateRoutineRequest request);

  @GetMapping("/routines")
  ResponseEntity<List<RoutineDTO>> listAllFavoritesAdmin();

  @GetMapping("/routines/{id}")
  ResponseEntity<RoutineFullResponse> getById(@PathVariable Long id);

  @PutMapping("/routines/{id}")
  ResponseEntity<RoutineDTO> update(@PathVariable Long id, @RequestBody CreateRoutineRequest request);

  @PostMapping("/accounts/{accountId}/routines/{routineId}")
  ResponseEntity<RoutineDTO> cloneRoutineAndLinkAccount(@PathVariable Long accountId, @PathVariable Long routineId);

  @DeleteMapping("/routines/{id}")
  ResponseEntity<Void> deleteById(@PathVariable Long id);

}
