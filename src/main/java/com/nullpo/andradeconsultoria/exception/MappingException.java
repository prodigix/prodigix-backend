package com.nullpo.andradeconsultoria.exception;

public class MappingException extends RuntimeException {
  public MappingException(String message) {
    super(message);
  }
}
