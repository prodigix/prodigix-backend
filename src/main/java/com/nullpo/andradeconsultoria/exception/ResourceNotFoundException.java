package com.nullpo.andradeconsultoria.exception;

public class ResourceNotFoundException extends RuntimeException {

  private static final String DEFAULT_MESSAGE = "Resource not found | ";

  public ResourceNotFoundException(String message) {
    super(message);
  }

  public ResourceNotFoundException(String resourceName, Long id) {
    super(DEFAULT_MESSAGE + resourceName + id);
  }
}
