package com.nullpo.andradeconsultoria.repository;

import com.nullpo.andradeconsultoria.model.WorkoutExerciseDetail;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface WorkoutExerciceDetailsRepository extends JpaRepository<WorkoutExerciseDetail, Long> {
  WorkoutExerciseDetail findByWorkoutIdAndExerciseId(Long workoutId, Long exerciseId);

  List<WorkoutExerciseDetail> findByWorkoutId(Long workoutId);
}
