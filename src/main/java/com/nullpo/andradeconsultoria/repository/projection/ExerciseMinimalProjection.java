package com.nullpo.andradeconsultoria.repository.projection;

public interface ExerciseMinimalProjection {
  Long getId();
  String getName();
  String getLink();
  MuscleGroupProjection getMuscleGroup();
}
