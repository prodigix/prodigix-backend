package com.nullpo.andradeconsultoria.repository.projection;

import com.nullpo.andradeconsultoria.model.enumeration.Plans;

import java.time.LocalDate;

public interface AccountMinimalProjection {
  Long getId();
  String getName();
  String getEmail();
  String getPhoto();
  String getDisplayName();
  String getWhatsapp();
  LocalDate getExpirationDate();
  Plans getPlan();
}
