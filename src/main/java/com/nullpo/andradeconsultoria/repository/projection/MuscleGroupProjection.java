package com.nullpo.andradeconsultoria.repository.projection;

public interface MuscleGroupProjection {
  Long getId();
  String getName();
}
