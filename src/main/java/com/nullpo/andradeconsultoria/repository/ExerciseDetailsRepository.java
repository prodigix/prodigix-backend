package com.nullpo.andradeconsultoria.repository;

import com.nullpo.andradeconsultoria.model.ExerciseDetails;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ExerciseDetailsRepository extends JpaRepository<ExerciseDetails, Long> {
  Optional<ExerciseDetails> findBySetsRepetitionLoadInterval(String setsRepetitionLoadInterval);
}
