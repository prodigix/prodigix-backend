package com.nullpo.andradeconsultoria.repository;

import com.nullpo.andradeconsultoria.model.Exercise;
import com.nullpo.andradeconsultoria.repository.projection.ExerciseMinimalProjection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ExerciseRepository extends JpaRepository<Exercise, Long> {
  @Query("SELECT new com.nullpo.andradeconsultoria.dto.ExerciseMinimalDTO(e.id, e.name, e.link, new com.nullpo.andradeconsultoria.dto.MuscleGroupDTO(mg.id, mg.name)) " +
    "  FROM Exercise e " +
    "  JOIN e.muscleGroup mg " +
    " WHERE e.id NOT IN (SELECT wed.exercise.id " +
    "                      FROM WorkoutExerciseDetail wed " +
    "                     WHERE wed.workout.id = :ignoredWorkoutId)")
  List<ExerciseMinimalProjection> findAllNotInWorkout(@Param("ignoredWorkoutId") Long ignoredWorkoutId);

  @Query("SELECT new com.nullpo.andradeconsultoria.dto.ExerciseMinimalDTO(e.id, e.name, e.link, new com.nullpo.andradeconsultoria.dto.MuscleGroupDTO(mg.id, mg.name)) FROM Exercise e JOIN e.muscleGroup mg")
  List<ExerciseMinimalProjection> findAllMinimal();

}
