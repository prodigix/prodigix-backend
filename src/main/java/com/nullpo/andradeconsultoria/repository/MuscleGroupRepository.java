package com.nullpo.andradeconsultoria.repository;

import java.util.List;
import java.util.Optional;

import com.nullpo.andradeconsultoria.dto.MuscleGroupDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nullpo.andradeconsultoria.model.MuscleGroup;

@Repository
public interface MuscleGroupRepository extends JpaRepository<MuscleGroup, Long> {
  Optional<MuscleGroup> findByName(String name);
  List<MuscleGroupDTO> findByNameContaining(String name);
}
