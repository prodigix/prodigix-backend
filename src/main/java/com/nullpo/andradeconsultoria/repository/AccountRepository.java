package com.nullpo.andradeconsultoria.repository;

import com.nullpo.andradeconsultoria.model.Account;
import com.nullpo.andradeconsultoria.model.enumeration.Plans;
import com.nullpo.andradeconsultoria.repository.projection.AccountMinimalProjection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.Set;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {

  Set<AccountMinimalProjection> findByPlanIsNot(Plans plan);

  Optional<Account> findByEmail(String email);
}
