package com.nullpo.andradeconsultoria.repository;

import com.nullpo.andradeconsultoria.model.Workout;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface WorkoutRepository extends JpaRepository<Workout, Long> {
  @Modifying
  @Query("DELETE FROM WorkoutExerciseDetail wed WHERE wed.workout.id = :id")
  void deleteWorkoutExerciseDetailsByWorkoutId(@Param("id") Long id);

  @Modifying
  @Query("DELETE FROM Workout w WHERE w.id = :id")
  void deleteWorkout(@Param("id") Long id);
}
