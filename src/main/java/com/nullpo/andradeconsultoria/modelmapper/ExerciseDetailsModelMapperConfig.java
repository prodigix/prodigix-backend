package com.nullpo.andradeconsultoria.modelmapper;

import com.nullpo.andradeconsultoria.dto.ExerciseDetailsDTO;
import com.nullpo.andradeconsultoria.model.ExerciseDetails;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ExerciseDetailsModelMapperConfig implements MappingConfiguration {

  Converter<ExerciseDetails, ExerciseDetailsDTO> toExerciseDetailsDTO = context -> {
    ExerciseDetails source = context.getSource();
    ExerciseDetailsDTO destination = context.getDestination();

    if (destination == null) {
      destination = new ExerciseDetailsDTO();
    }

    if (source != null) {
      String[] attributes = source.getSetsRepetitionLoadInterval().split(",");
      destination.setSets(attributes[0].trim());
      destination.setRepetition(attributes[1].trim());
      destination.setLoad(attributes[2].trim());
      destination.setIntervalSeconds(attributes[3].trim());
    }
    return destination;
  };

  Converter<ExerciseDetailsDTO, ExerciseDetails> toEntity = context -> {
    ExerciseDetailsDTO source = context.getSource();
    ExerciseDetails destination = context.getDestination();

    if (destination == null) {
      destination = new ExerciseDetails();
    }

    if (source != null) {
      String joinAttributesString = source.getSets() + ","
        + source.getRepetition() + ","
        + source.getLoad() + ","
        + source.getIntervalSeconds();
      destination.setSetsRepetitionLoadInterval(joinAttributesString);
    }
    return destination;
  };

  @Override
  public void configure(ModelMapper modelMapper) {
    modelMapper.createTypeMap(ExerciseDetails.class, ExerciseDetailsDTO.class).setConverter(toExerciseDetailsDTO);
    modelMapper.createTypeMap(ExerciseDetailsDTO.class, ExerciseDetails.class).setConverter(toEntity);
  }
}
