package com.nullpo.andradeconsultoria.modelmapper;

import org.modelmapper.ModelMapper;

public interface MappingConfiguration {
  void configure(ModelMapper modelMapper);
}
