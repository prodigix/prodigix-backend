package com.nullpo.andradeconsultoria.modelmapper;

import com.nullpo.andradeconsultoria.controller.response.ExerciseDetailsResponse;
import com.nullpo.andradeconsultoria.dto.ExerciseDetailsDTO;
import com.nullpo.andradeconsultoria.dto.WorkoutDTO;
import com.nullpo.andradeconsultoria.model.Workout;
import com.nullpo.andradeconsultoria.model.WorkoutExerciseDetail;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.springframework.context.annotation.Configuration;

import java.util.HashSet;

@Configuration
public class WorkoutModelMapperConfig implements MappingConfiguration {

  @Override
  public void configure(ModelMapper modelMapper) {

    Converter<Workout, WorkoutDTO> toWorkoutDTO = context -> {
      Workout source = context.getSource();
      WorkoutDTO destination = context.getDestination();

      if (destination == null) {
        destination = new WorkoutDTO();
        destination.setExercises(new HashSet<>());
      }

      if (source != null) {
        destination.setId(source.getId());
        destination.setName(source.getName());
        destination.setInfo(source.getInfo());
        destination.setRoutineId(source.getRoutine().getId());
        for (WorkoutExerciseDetail wed : source.getWorkoutExerciseDetail()) {
          ExerciseDetailsResponse exerciseDetailsResponse = new ExerciseDetailsResponse();
          exerciseDetailsResponse.setId(wed.getExercise().getId());
          exerciseDetailsResponse.setName(wed.getExercise().getName());
          exerciseDetailsResponse.setMuscleGroup(wed.getExercise().getMuscleGroup().getName());
          exerciseDetailsResponse.setLink(wed.getExercise().getLink());
          exerciseDetailsResponse.setGuideLines(wed.getExercise().getGuideLines());
          exerciseDetailsResponse.setDetails(modelMapper.map(wed.getDetails(), ExerciseDetailsDTO.class));
          destination.getExercises().add(exerciseDetailsResponse);
        }
      }

      return destination;
    };

    modelMapper.createTypeMap(Workout.class, WorkoutDTO.class).setConverter(toWorkoutDTO);

    modelMapper.addMappings(new PropertyMap<WorkoutDTO, Workout>() {
      @Override
      protected void configure() {
        map().getRoutine().setId(source.getRoutineId());
      }
    });
  }
}
