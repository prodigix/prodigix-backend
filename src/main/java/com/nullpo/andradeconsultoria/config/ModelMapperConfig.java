package com.nullpo.andradeconsultoria.config;

import com.nullpo.andradeconsultoria.modelmapper.MappingConfiguration;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
public class ModelMapperConfig {
  @Bean
  public ModelMapper modelMapper(List<MappingConfiguration> mappingConfigurations) {
    ModelMapper modelMapper = new ModelMapper();
    mappingConfigurations.forEach(config -> config.configure(modelMapper));
    return modelMapper;
  }
}
