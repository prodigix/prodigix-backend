package com.nullpo.andradeconsultoria.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.nullpo.andradeconsultoria.auth.model.AccountRole;
import com.nullpo.andradeconsultoria.model.enumeration.Genders;
import com.nullpo.andradeconsultoria.model.enumeration.Goals;
import com.nullpo.andradeconsultoria.model.enumeration.Plans;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Account {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  private String name;
  private String photo;
  private String lastName;
  private String displayName;
  private String whatsapp;
  private LocalDate birthday;
  private Genders gender;
  private Plans plan;
  private double weight;
  private double height;
  private Goals goal;
  private LocalDate expirationDate;
  @OneToMany(mappedBy = "account", fetch = FetchType.EAGER)
  private List<Routine> routines;

  private String email;
  @JsonIgnore
  private String password;
  @ManyToMany(fetch = FetchType.EAGER)
  private Set<AccountRole> roles = new HashSet<>();
}
