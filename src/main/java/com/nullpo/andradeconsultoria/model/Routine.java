package com.nullpo.andradeconsultoria.model;

import java.time.LocalDate;
import java.util.Set;

import com.nullpo.andradeconsultoria.model.enumeration.Difficulties;
import com.nullpo.andradeconsultoria.model.enumeration.Goals;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Routine {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  private String name;
  private Goals goal;
  private Difficulties difficulty;
  private LocalDate initDate;
  private LocalDate endDate;

  @OneToMany(mappedBy = "routine", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
  private Set<Workout> workouts;

  @ManyToOne
  @JoinColumn(name = "account_id")
  private Account account;

}
