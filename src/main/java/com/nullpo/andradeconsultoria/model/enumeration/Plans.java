package com.nullpo.andradeconsultoria.model.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum Plans {
  ADMIN("ROLE_ADMIN"),
  NORMAL_STUDENT("ROLE_NORMAL_STUDENT"),
  SKULL_STUDENT("ROLE_SKULL_STUDENT"),
  EXPIRED("ROLE_DEACTIVATED_STUDENT");

  private final String value;
}
