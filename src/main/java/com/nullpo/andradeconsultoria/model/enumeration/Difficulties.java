package com.nullpo.andradeconsultoria.model.enumeration;

public enum Difficulties {
  BEGINNER,
  INTERMEDIATE,
  ADVANCED;
}
