package com.nullpo.andradeconsultoria.model.enumeration;

public enum Genders {
  MALE,
  FEMALE;
}
