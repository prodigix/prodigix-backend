package com.nullpo.andradeconsultoria.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Exercise {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  private String name;
  @ManyToOne
  @JoinColumn(name = "muscle_group_id")
  private MuscleGroup muscleGroup;
  private String link;
  @Lob
  private String guideLines;

  @OneToMany(mappedBy = "exercise")
  private List<WorkoutExerciseDetail> workoutExerciseDetails;

}
