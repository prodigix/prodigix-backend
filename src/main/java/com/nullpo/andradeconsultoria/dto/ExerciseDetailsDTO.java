package com.nullpo.andradeconsultoria.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ExerciseDetailsDTO {
  private String sets;
  private String repetition;
  private String load;
  private String intervalSeconds;
}
