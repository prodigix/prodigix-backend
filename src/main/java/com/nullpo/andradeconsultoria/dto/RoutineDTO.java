package com.nullpo.andradeconsultoria.dto;

import com.nullpo.andradeconsultoria.model.enumeration.Difficulties;
import com.nullpo.andradeconsultoria.model.enumeration.Goals;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RoutineDTO {
  private Long id;
  private String name;
  private Goals goal;
  private Difficulties difficulty;
  private LocalDate initDate;
  private LocalDate endDate;
}
