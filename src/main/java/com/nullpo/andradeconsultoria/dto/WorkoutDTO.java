package com.nullpo.andradeconsultoria.dto;

import com.nullpo.andradeconsultoria.controller.response.ExerciseDetailsResponse;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class WorkoutDTO {
  private Long id;
  private String name;
  private String info;
  private Set<ExerciseDetailsResponse> exercises;
  private Long routineId;
}
