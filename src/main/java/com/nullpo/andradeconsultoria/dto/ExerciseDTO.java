package com.nullpo.andradeconsultoria.dto;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ExerciseDTO {
  private Long id;
  @NotBlank
  private String name;
  @NotBlank
  private String muscleGroup;
  @NotBlank
  private String link;
  @NotBlank
  private String guideLines;
}
