package com.nullpo.andradeconsultoria.dto;

import com.nullpo.andradeconsultoria.model.enumeration.Genders;
import com.nullpo.andradeconsultoria.model.enumeration.Goals;
import com.nullpo.andradeconsultoria.model.enumeration.Plans;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AccountDTO {
  private Long id;
  private String email;
  private String name;
  private String photo;
  private String lastName;
  private String displayName;
  private String whatsapp;
  private LocalDate birthday;
  private Genders gender;
  private Plans plan;
  private double weight;
  private double height;
  private Goals goal;
  private LocalDate expirationDate;
}
